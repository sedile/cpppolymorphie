#pragma once

#include "Figur.h"

class Kreis : public Figur {
private:
    const double PI = 3.1415;
public:
    explicit Kreis();
    virtual ~Kreis();

    std::string getFigurenname() const override;
    void setParameter(const double &p) override;
    double getArea() const override;
};
