#include <iostream>

#include "Polygon.h"
#include "Dreieck.h"
#include "Kreis.h"
#include "Quadrat.h"

/* Vererbungsbaum */
//                  Figur
//         Polygon         Kreis
//  Dreieck     Quadrat

int main() {

    /* ohne polymorphie */
    Dreieck d;
    std::string d_name = d.getFigurenname();
    std::cout << d_name << "\n\n";

    /* mit polymorphie */
    /* Erzeuge links eine Basisklasse und rechts eine Subklasse dieser Basisklasse */
    Figur *figures[3];
    figures[0] = new Dreieck();
    figures[1] = new Quadrat();
    figures[2] = new Kreis();

    double p = 5;
    for(unsigned short i = 0; i < 3; i++){
        figures[i]->setParameter(p);

        /* Mit polymorphie koennen gemeinsame Funktionen innerhalb der Vererbungshierarchie aufgerufen werden */
        std::cout << figures[i]->getFigurenname() << " : " << figures[i]->getArea() << "\n";
    }

    for(unsigned short i = 0; i < 3; i++){
        delete figures[i];
    }

    return 0;
}
