#include "Quadrat.h"

Quadrat::Quadrat() : Polygon(4) { }

Quadrat::~Quadrat() { }

std::string Quadrat::getFigurenname() const {
    return "Quadrat";
}

double Quadrat::getUmfang(double *args) {
    return args[0] + args[1] + args[2] + args[3];
}

void Quadrat::setParameter(const double &p) {
    area = p;
}

double Quadrat::getArea() const {
    return area * area;
}
