#include "Figur.h"

Figur::Figur(bool def) :
    definiert(def),
    area(0.0)
    {}

Figur::~Figur() { }

bool Figur::getDefiniert() const {
    return definiert;
}

std::string Figur::getFigurenname() const {
    return "<unbekannt>";
}
