#include "Polygon.h"

Polygon::Polygon(unsigned int pts) :
    Figur(true),
    punkte(pts)
    {}

Polygon::~Polygon() { }

unsigned int Polygon::getAnzahlPunkte() const {
    return punkte;
}
